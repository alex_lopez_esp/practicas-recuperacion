package Ejercicio2;

import java.util.ArrayList;

public class Ejercicio2 {
    private final int NUM_WRITERS=5;
    private final int NUM_READERS=3;

    ArrayList<Thread> encargadosdeEscribir =new ArrayList<>();
    ArrayList<Thread> encargadosDeLeer=new ArrayList<>();

    public static void main(String[] args)throws InterruptedException {
        Ejercicio2 ejercicio2=new Ejercicio2();
        ColaDeMensajes colaDeMensajes=new ColaDeMensajes();

        for (int i = 0; i < ejercicio2.NUM_WRITERS; i++) {
            ejercicio2.encargadosdeEscribir.add(new Thread(new Writer((i+1),"Mensaje del Escrito "+(i+1),colaDeMensajes)));
            ejercicio2.encargadosdeEscribir.get(i).start();
            if (i<ejercicio2.NUM_READERS){
                ejercicio2.encargadosDeLeer.add(new Thread(new Reader((i+1),colaDeMensajes)));
                ejercicio2.encargadosDeLeer.get(i).start();
            }
        }

        for(int i = 0 ; i < ejercicio2.encargadosdeEscribir.size();i++){
            ejercicio2.encargadosdeEscribir.get(i).join();
            if(i < ejercicio2.NUM_READERS){
                ejercicio2.encargadosDeLeer.get(i).join();
            }
        }

    }
}

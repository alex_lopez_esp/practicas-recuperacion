package Ej5;

import java.io.*;

public class Adder {
    public static void main(String[] args) {
     int total = 0;
        if (args.length <= 0){
            System.out.println("No tiene argumentos para leer");
        }else {
            try {

                File fichero = new File(args[0]);
                BufferedReader br = new BufferedReader(new FileReader(fichero));
                String linia;
                while ((linia = br.readLine()) != null) {
                    total += Integer.parseInt(linia);
                }
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(total);
        }
    }
}

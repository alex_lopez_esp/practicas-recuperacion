package Ejercicio1;

import java.util.Scanner;

public class Ejercicio1 {
    public static final Scanner scanner=new Scanner(System.in);
    private static Thread[] hilos;
    Total total=new Total();

    public static void main(String[] args) {
        Ejercicio1 ejercicio1=new Ejercicio1();
        ejercicio1.crearThreads(numeroDeHilos(),preguntarvalorASumar());
        ejercicio1.total.mostrarContenido();
    }

    public static int numeroDeHilos(){
        int numeroHilos;
        do {
            System.out.print("Introduce el numero de hilos que quieres que se ejecuten [50-100]: ");
            numeroHilos=scanner.nextInt();
            if (numeroHilos<50 || numeroHilos>100){
                System.out.println("Error el numero introducido no es valido");
            }


        }while (numeroHilos<50 || numeroHilos>100);
        return numeroHilos;
    }
    public static int preguntarvalorASumar(){
        int valor = 0;
        do {
            System.out.print("Introduce un valor: ");
            if (scanner.hasNextInt()){
                valor=scanner.nextInt();
            }else {
                System.out.println("Error el dato introducido no es valido");
            }

        }while (valor<=0);
        return valor;
    }

    public void crearThreads(int numeroThreads,int valorDelThread){
        hilos=new Thread[numeroThreads];

        for (int i = 0; i < hilos.length; i++) {
            hilos[i]=new Thread(new Calcular(valorDelThread,total),"Hilo numero "+(i+1));
            hilos[i].start();
        }
        for (int x = 0; x < hilos.length; x++) {
            try {
                hilos[x].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

}

package Ej5;

import java.io.*;
import java.util.*;

public class Ejercicio5 {

    private static long total;

    public static void main(String[] args) {
        try {

            String acceso = "java -jar RecuperacionPracticas.jar";
            if (args == null) {
                System.out.println("Son necesarios los argumentos");
            } else {
                StringBuilder stringBuilder = new StringBuilder();
                List<String> contenido = new ArrayList<>();

                Collections.addAll(contenido, args);
                String suma;
                for (String s : contenido) {
                    List<String> comando = new ArrayList<String>(Arrays.asList(acceso.split(" ")));
                    comando.add(s);
                    ProcessBuilder pb = new ProcessBuilder(comando);
                    Process adder = pb.start();
                    Scanner scanner=new Scanner(adder.getInputStream());
                    System.out.println();
                    suma = scanner.nextLine();
                    total += Integer.parseInt(suma);
                    stringBuilder.append("El total del archivo es :").append(suma).append(System.lineSeparator());
                    System.out.println(stringBuilder);

                }
            }
            File file=new File("Totals.txt");
            BufferedWriter bw=new BufferedWriter(new FileWriter(file));
            bw.write("El total es :"+total);
            bw.newLine();
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

package Ejercicio2;

public class Writer implements Runnable{
    int numero;
    String mensaje;
    ColaDeMensajes colaDeMensajes;
    private final int MAX_WRITTEN_MESSAGES=10;

    public Writer(int numero, String mensaje, ColaDeMensajes colaDeMensajes) {
        this.numero = numero;
        this.mensaje = mensaje;
        this.colaDeMensajes = colaDeMensajes;
    }

    @Override
    public void run() {
        while (colaDeMensajes.getContador()<=MAX_WRITTEN_MESSAGES){
            colaDeMensajes.put(mensaje);


        }
    }
}

package Ej4;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Traductor {
    private static final Map<String, String> DICCIONARIO = new HashMap<String, String>();
    private static final Map<String, String> DICCIONARIOEXTERNO = new HashMap<String, String>();

    public static void main(String[] args) {
        String linea;
        Scanner scanner=new Scanner(System.in);
        if (args.length>0){
            linea=scanner.nextLine();
            if (comprobar(args)){
                while (!linea.equals("fin")){
                    System.out.println(DICCIONARIO.getOrDefault(linea,"desconocido"));
                    linea=scanner.nextLine();
                }
            }else {
                diccionarioInterno();
                linea=scanner.nextLine();
                while (!linea.equals("fin")){
                    System.out.println(DICCIONARIO.getOrDefault(linea,"desconocido"));
                    linea=scanner.nextLine();
                }
            }
        }else {
            diccionarioInterno();
            linea=scanner.nextLine();
            while (!linea.equals("fin")){
                System.out.println(DICCIONARIO.getOrDefault(linea,"desconocido"));
                linea=scanner.nextLine();
            }
        }
    }



    private static boolean comprobar(String[] args){
        boolean comprobante=false;
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-d")){
                comprobante=true;
            }

        }
        return comprobante;
    }
    private static void dicExterno(){
        String ruta="Diccionario.txt";
        File file=new File(ruta);
        FileReader fileReader;
        BufferedReader bufferedReader;
        try{
            bufferedReader=new BufferedReader(new FileReader(file));
            String linia;
            do {
                linia=bufferedReader.readLine();
                if (linia!=null) {
                    String[] valores = linia.split(" ");
                    DICCIONARIOEXTERNO.put(valores[0], valores[1]);
                }
            }while (linia!=null);
            bufferedReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void diccionarioInterno() {
        DICCIONARIO.put("us", "nosotros");
        DICCIONARIO.put("most", "la mayoria");
        DICCIONARIO.put("day", "día");
        DICCIONARIO.put("give", "dat");
        DICCIONARIO.put("these", "estos , estas");
        DICCIONARIO.put("any", " alguno/a, cualquier/a");
        DICCIONARIO.put("because", "porque");
        DICCIONARIO.put("want", "querer");
        DICCIONARIO.put("new", "nuevo");
        DICCIONARIO.put("even", "incluso, aún, parejo");
        DICCIONARIO.put("way", "camino, manera, método");
        DICCIONARIO.put("well", "bien");
        DICCIONARIO.put("first", "primero/a");
        DICCIONARIO.put("work", "trabajo, trabajar");
        DICCIONARIO.put("our", "nuestro/a");
        DICCIONARIO.put("how", "cómo, como");
        DICCIONARIO.put("two", "dos");
        DICCIONARIO.put("use", "usar, uso");
        DICCIONARIO.put("after", "después de");
        DICCIONARIO.put("back", "de vuelta, atrás, espalda,");
        DICCIONARIO.put("also", "también");
        DICCIONARIO.put("think", "pensar");
        DICCIONARIO.put("over", "encima de, por encima de, más de");
        DICCIONARIO.put("its", "su");
        DICCIONARIO.put("come", "venir, llegar");
        DICCIONARIO.put("only", "solo, solamente, únicamente");
        DICCIONARIO.put("look", "mirar, buscar, parecer");
        DICCIONARIO.put("now", "ahora");
        DICCIONARIO.put("then", "entonces");
        DICCIONARIO.put("than", "que");
        DICCIONARIO.put("other", "otra/o");
        DICCIONARIO.put("see", "ver");
        DICCIONARIO.put("them", "ellos, los (posesivo)");
        DICCIONARIO.put("could", "podría, podríamos, podrían");
        DICCIONARIO.put("some", "algo, alguno, algunas");
        DICCIONARIO.put("good", "bueno");
        DICCIONARIO.put("your", "tu/tus (posesivo)");
        DICCIONARIO.put("year", "año");
        DICCIONARIO.put("into", "dentro de, en, contra");
        DICCIONARIO.put("people", "gente, personas");
        DICCIONARIO.put("take", "tomar");

    }
}

package Ejercicio2;

import java.util.LinkedList;
import java.util.Queue;

public class ColaDeMensajes {
    private int contador;
    private final int QUEUE_SIZE=5;
    Queue<String> threadsQueue=new LinkedList<>();



    public synchronized  void put(String mensaje){
        while (threadsQueue.size()>QUEUE_SIZE){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        threadsQueue.offer(mensaje);
        contador++;
        notifyAll();

    }
    public synchronized String get(){
        while (threadsQueue.isEmpty()){
            try {


                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
        String mensaje=threadsQueue.element();
        threadsQueue.remove();
        notifyAll();
        return mensaje;
    }



    public int getContador(){
        return contador;
    }
}

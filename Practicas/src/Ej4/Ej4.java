package Ej4;

import Ej5.Ejercicio5;

import java.io.*;
import java.util.Scanner;

public class Ej4 {
    private static String EJECUTAR="java -jar Traductor.jar";
    public static void main(String[] args) {
        if (args.length<=0){
            System.out.println("Hacen falta argumentos");
        }else {
            ejercicio4(args);
        }
    }

    private static void ejercicio4(String[] args) {
       String nombreFichero="Traductor.txt";

        if (args.length<=0){
            noTieneArchivo(nombreFichero);

        }else if (args.length==2){
            if (args[0].equals("-d")){
                    noTieneArchivo(nombreFichero);

            }
               if (args[0].equals("-f")){
                tieneArchivo(nombreFichero,args);
                }
            } else if (args.length==4){
                tieneArchivo(nombreFichero,args);
        }
    }
    public static void noTieneArchivo(String nombreFichero){

        ProcessBuilder pb=new ProcessBuilder(EJECUTAR.split(" "));
        StringBuilder stringBuilder = null;
        Scanner scanner=new Scanner(System.in);
        try{
            Process process=pb.start();
            BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));

            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line=bufferedReader.readLine();
            String traducida, texto = "";
            while (!line.equals("fin")){
                bw.write(line);
                bw.newLine();
                bw.flush();
                traducida= scanner.nextLine();
                texto= texto+" "+line+" ->> "+ traducida+System.lineSeparator();
                line=bufferedReader.readLine();

            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        guardarInformacion(stringBuilder.toString(),nombreFichero);
    }
    public static void tieneArchivo(String nombre,String[] args){
        String linea,traducir;
        ProcessBuilder pb=new ProcessBuilder(EJECUTAR.split(" "));
        BufferedReader br;
        StringBuilder stringBuilder=null;
        try {
            Process traductor=pb.start();
            Scanner scanner=new Scanner(traductor.getInputStream());
            br=new BufferedReader(new FileReader(args[args.length-1]));

            BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(traductor.getOutputStream()));
            while ((linea=br.readLine())!=null){
                System.out.println(linea);
                bw.write(linea);
                bw.newLine();
                bw.flush();
                traducir= scanner.nextLine();
                System.out.println(traducir);
                stringBuilder.append(linea).append(" ->> ").append(traducir).append(System.lineSeparator());
            }
            bw.close();
            br.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
        guardarInformacion(stringBuilder.toString(),nombre);

    }

    public static void guardarInformacion(String nombreFichero,String info){
        try {


            BufferedWriter bufferedWriter = null;
            File file = new File(nombreFichero);
            try {


                bufferedWriter = new BufferedWriter(new FileWriter(file));
                bufferedWriter.write(info);
            } catch (IOException e) {
                e.printStackTrace();
            }
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

package Ejercicio2;

public class Reader implements Runnable{
    int numero;
    int contador;
    ColaDeMensajes colaDeMensajes;

    public Reader(int numero, ColaDeMensajes colaDeMensajes) {
        this.numero = numero;
        this.colaDeMensajes = colaDeMensajes;
    }

    @Override
    public void run() {
        while (!colaDeMensajes.threadsQueue.isEmpty()){
            System.out.println("Reader - "+numero+" : Esta Leyendo: "+colaDeMensajes.get());
            contador++;
        }

    }
}
